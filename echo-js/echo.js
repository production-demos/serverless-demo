"use strict"

module.exports = function echo (name) {

    if (typeof name == 'object') {
        // Riff's runtime gives you an object even if data is POSTed without Content-Type, but it's an odd object
        if (Object.keys(name).length === 1 && name[Object.keys(name)[0]] === "") {
            name = name[Object.keys(name)[0]];
        } else {
            name = JSON.stringify(name);
        }
    } else if (typeof name !== 'string') {
        console.warn('Got argument type', typeof name, ':', name);
        name = 'error: Unexpected argument type ' + (typeof name);
    }

    let envs = {}
    for (let [key, value] of Object.entries(process.env)) {
        if(key.startsWith('MY_')) {
            envs[key] = value
        }
    }
    
    return `${name} - ${JSON.stringify(envs)}`;

};
